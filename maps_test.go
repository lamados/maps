package maps

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var m = map[string]string{
	"key0": "value0",
	"key1": "value1",
	"key2": "value2",
	"key3": "value3",
}

func TestKeys(t *testing.T) {
	assert.Equal(t, Keys(m), []string{"key0", "key1", "key2", "key3"})
}

func TestValues(t *testing.T) {
	assert.Equal(t, Values(m), []string{"value0", "value1", "value2", "value3"})
}
