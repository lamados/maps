package maps

func Keys[T comparable, U any](m map[T]U) []T {
	ks := make([]T, len(m))
	i := 0

	for k := range m {
		ks[i] = k
		i++
	}

	return ks
}

func Values[T comparable, U any](m map[T]U) []U {
	vs := make([]U, len(m))
	i := 0

	for _, v := range m {
		vs[i] = v
		i++
	}

	return vs
}
